package com.firstgroupapp.test;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import java.util.Properties;
public class producer_1 {
    public static void main(String[] args) {
        // create_properties
        // String bootstrapServers="167.172.70.140:9992";
        String bootstrapServers="159.223.37.225:9993";
        Properties properties= new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        // create Producer
        KafkaProducer<String, String> first_producer = new KafkaProducer< String, String>(properties);
        // create Producer record
        ProducerRecord<String, String> record=new ProducerRecord<String, String>("test", "Testing sent successed");
        // sending the data
        first_producer.send(record);
        first_producer.flush();
        first_producer.close();
    }
}
